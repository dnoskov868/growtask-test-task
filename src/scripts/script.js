const slider = () => {
    const sliderImages = document.querySelectorAll('.slider-track img'),
        sliderTrack = document.querySelector('.slider-track'),
        sliderPrev = document.querySelector('.prev'),
        sliderNext = document.querySelector('.next');

    let sliderWidth = 0;
    const slideCounter = sliderImages.length - 1;

    let position = 0;
    let positionShoweSlide = 1;

    let movePosition = 1200;

    const setPosition = () => {
        sliderTrack.style.transform = `translateX(${position}px)`;

        console.log(position);
    };

    const setOpacity = () => {
        sliderImages.forEach((item, i) => {
            item.style.opacity = '0.3';
            if(positionShoweSlide === i) {
                item.style.opacity = '1';
            }
        });

        console.log(positionShoweSlide);
    };
    

    sliderImages.forEach(item => {
        sliderWidth += item.width;
    })

    setOpacity();

    sliderNext.addEventListener('click', () => {
        if (position === -1200) {
            position = 1200;
            positionShoweSlide = 0;
        } else {
            position -= movePosition;
            positionShoweSlide += 1;
        }
        
        setPosition();

        setOpacity();
    });

    sliderPrev.addEventListener('click', () => {
        if (position === 1200) {
            position = -1200;
            positionShoweSlide = slideCounter;
        } else {
            position += movePosition;
            positionShoweSlide -= 1; 
        }
        
        setPosition();

        setOpacity();
    });
};

slider();